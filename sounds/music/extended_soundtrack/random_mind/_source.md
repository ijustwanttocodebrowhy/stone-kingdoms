All tracks were made by Random Mind (www.patreon.com/randommind)
Licensed under CC0
https://creativecommons.org/publicdomain/zero/1.0/

---

Title: Random Mind - The Bard's Tale
Source URI: https://drive.google.com/file/d/1YW0DNJXGw2vUQAH5wrM8d7IkJymwCmHB/view
Source website: http://patreon.com/randommind
Licence: CC0
Description: "My fantasy track! You can use it for free without giving any credits! "

Title: Random Mind - The Old Tower Inn
Source URI: https://drive.google.com/file/d/1J-EUFapO1yxXb67medXoshmZ19wdsfY0/view
Source website: https://patreon.com/randommind
Licence: CC0
Description: "My medieval theme that you can use in your projects, both personal and commercial. Credits are appreciated but not required."
