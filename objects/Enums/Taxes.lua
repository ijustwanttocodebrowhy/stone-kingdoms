---@enum taxes
local TAXES = {
    GenerousBribe = "GenerousBribe",
    LargeBribe = "LargeBribe",
    SmallBribe = "SmallBribe",
    NoTaxes = "NoTaxes",
    LowTaxes = "LowTaxes",
    AverageTaxes = "AverageTaxes",
    HighTaxes = "HighTaxes",
    MeanTaxes = "MeanTaxes",
    ExtortionateTaxes = "ExtortionateTaxes",
    DownrightCruelTaxes = "DownrightCruelTaxes",
}

return TAXES
